from django.contrib import admin
from TrackerApp.models import *
# Register your models here.

admin.site.register(Coach)
admin.site.register(Group)
admin.site.register(Member)
admin.site.register(Practice)
admin.site.register(ActivityNumber)
admin.site.register(Activity)
