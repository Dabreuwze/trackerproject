from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.conf import settings
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget

import os
from TrackerApp import models


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional')
    email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
    # captcha = ReCaptchaField(widget=ReCaptchaWidget())

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2',)
        widgets = {
            # 'username': forms.TextInput(attrs={'class':'form-control', 'placeholder': 'UserName'}),
            'first_name': forms.TextInput(attrs={'class':'form-control', 'placeholder':'First Name'}),
            'last_name': forms.TextInput(attrs={'class':'form-control', 'placeholder':'Last Name'}),
            'email': forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Email'}),
        }


class GroupCreateForm(forms.ModelForm):
    model = models.Group
    widgets = {
        'name': forms.TextInput(),
        'description': forms.TextInput(),
    }

    help_texts = {
        'name': ("Name of the Group"),
        'description': ("Description of group"),
    }

class ActivityCreateForm(forms.ModelForm):
    class Meta:
        model = models.Activity
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(),
            'description': forms.TextInput(),
            # forms.ChoiceField(widget= forms.Select(attrs={'class': 'form-control'})),
        }

        help_texts = {
            'name': ("Name of the Activity"),
            'description': ("Description of the activity"),
        }
