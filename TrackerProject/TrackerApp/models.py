from django.db import models
import datetime, requests

from django.core import validators
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse

from django.db.models.signals import post_save

from django.dispatch import receiver
from django.contrib.auth.models import User

# Create your models here.
class Coach(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(blank=True)

    @receiver(post_save, sender=User)
    def update_user_profile(sender, instance, created, **kwargs):
        if created:
            Coach.objects.create(user=instance)
        instance.coach.save()

    def __str__(self):
        return self.user.username

class Group(models.Model):
    name = models.CharField(max_length = 32)
    description = models.TextField()
    coaches = models.ManyToManyField(Coach)
    members = models.ManyToManyField('Member',blank = True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("profile")


class Member(models.Model):
    first_name = models.CharField(max_length = 32)
    last_name = models.CharField(max_length = 32)
    birth_date = models.DateField(max_length = 32)

    def __str__(self):
        return self.last_name+", "+self.first_name

    def get_absolute_url(self):
        return reverse("profile")
        # return reverse("TrackerApp:group_detail")

class Practice(models.Model):
    name = models.CharField(max_length = 32)
    description = models.TextField()
    date = models.DateField()
    attendance = models.ManyToManyField(Member,blank = True)
    group = models.ForeignKey(Group)

    def __str__(self):
        return self.name+str(self.date)

    def get_absolute_url(self):
        return reverse("profile")
        # return reverse("TrackerApp:group_detail")

class ActivityNumber(models.Model):
    activity = models.ForeignKey('Activity')
    amount = models.IntegerField()
    practice = models.ForeignKey(Practice)

    def __str__(self):
        return str(self.practice.date)+": "+self.activity.name+" ("+str(self.amount)+")"

    def get_absolute_url(self):
        return reverse("profile")
        # return reverse("TrackerApp:group_detail")

class Activity(models.Model):
    name = models.CharField(max_length = 32)
    description = models.TextField()
    group = models.ForeignKey(Group)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("profile")
        # return reverse("TrackerApp:group_detail")
