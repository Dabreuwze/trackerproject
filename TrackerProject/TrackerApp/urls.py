from django.conf.urls import url
from TrackerApp import views

app_name ='TrackerApp'

urlpatterns= [
    url(r'^group_create', views.GroupCreate.as_view(),name='group_create'),
    url(r'^group_detail/(?P<pk>[-\w]+)/$', views.GroupDetail.as_view(),name='group_detail'),
    url(r'^group_update/(?P<pk>[-\w]+)/$', views.GroupUpdate.as_view(),name='group_update'),
    url(r'^member_create/(?P<slug>[-\w]+)/$', views.MemberCreate.as_view(),name='member_create'),
    url(r'^member_detail/(?P<pk>[-\w]+)/$', views.MemberDetail.as_view(),name='member_detail'),
    url(r'^member_update/(?P<pk>[-\w]+)/$', views.MemberUpdate.as_view(),name='member_update'),
    url(r'^practice_create/(?P<slug>[a-zA-Z0-9-]+)/$', views.PracticeCreate.as_view(),name='practice_create'),
    url(r'^practice_detail/(?P<pk>[-\w]+)/$', views.PracticeDetail.as_view(),name='practice_detail'),
    url(r'^practice_update/(?P<pk>[-\w]+)/$', views.PracticeUpdate.as_view(),name='practice_update'),
    url(r'^activity_create', views.ActivityCreate.as_view(),name='activity_create'),
    url(r'^activity_detail/(?P<pk>[-\w]+)/$', views.ActivityDetail.as_view(), name='activity_detail'),
    url(r'^activity_update/(?P<pk>[-\w]+)/$', views.ActivityUpdate.as_view(), name='activity_update'),
]

api_patterns = [

]

urlpatterns += api_patterns
