from django.shortcuts import render
from django.views.generic import (View, TemplateView, CreateView, DeleteView, UpdateView, DetailView)
from TrackerApp import models, forms, tokens
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import EmailMessage
from django.http import HttpResponseRedirect
# Create your views here.
#home page
class IndexView(TemplateView):
    template_name = 'TrackerApp/index.html'


class CoachDetailView(DetailView):
    model = models.Coach
    fields = '__all__'
    template_name = 'TrackerApp/coach_detail.html'

def user_profile(request):
    user_groups = None
    template_name = 'TrackerApp/coach_detail.html'
    if request.user.is_authenticated():
        user_groups = models.Group.objects.filter(coaches__user = request.user)
        context = {
            'user_groups':user_groups,
        }
    else:
        context = None
    return render(request,template_name,context)

class GroupCreate(CreateView):
    model = models.Group
    fields = ['name','description','members','coaches']
    template_name = 'TrackerApp/group_create.html'


class GroupDetail(DetailView):
    model = models.Group
    fields = "__all__"
    template_name = 'TrackerApp/group_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['group_members'] = models.Member.objects.filter(group = self.get_object())
        context['group_practices'] = models.Practice.objects.filter(group = self.get_object())
        context['group_activities'] = models.Activity.objects.filter(group = self.get_object())
        context['total_members'] = (len(self.get_object().members.all()))
        context['total_acts'] = len(models.Activity.objects.filter(group = self.get_object()))
        context['total_prac'] = len(models.Practice.objects.filter(group = self.get_object()))
        return context

class GroupUpdate(UpdateView):
    model = models.Group
    fields = "__all__"
    template_name = 'TrackerApp/group_update.html'

class MemberCreate(CreateView):
    model = models.Member
    fields = '__all__'
    template_name = "TrackerApp/member_create.html"

    def form_valid(self, form):
        response = super(MemberCreate, self).form_valid(form)
        group_pk = self.kwargs['slug']
        group = models.Group.objects.get(pk = group_pk)
        group.members.add(self.object)
        group.save()
        return response


class MemberDetail(DetailView):
    model = models.Member
    fields = '__all__'
    template_name = "TrackerApp/member_detail.html"

# groups apart of
#activities they have participated
#filter by week,month, two months
#attendence rate for each Groups
#upcoming practices?
#field to keep notes on player?

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # get all groups
            # for each group get all practices with user in attendance
                # for each practice add to total sum of each activity in dict amount
        context["Group_Stats"] = dict()
        for group in self.object.group_set.all():
            context["Group_Stats"][group.pk]= dict()
            context["Group_Stats"][group.pk]["name"] = dict()
            context["Group_Stats"][group.pk]["attendance"] = dict()
            context["Group_Stats"][group.pk]["activity_totals"]=dict()
            for group_practice in group.practice_set.all():
                print(group_practice)
                for activity_num in group_practice.activitynumber_set.all():
                    print(activity_num)
                    context["Group_Stats"][group.pk]["name"] = group.name
                    context["Group_Stats"][group.pk]["attendance"] = len(models.Practice.objects.filter(group__pk = group.pk).filter(attendance__id = self.object.pk))
                    context["Group_Stats"][group.pk]["total_practices"] = len(models.Practice.objects.filter(group__pk = group.pk))
                    if not activity_num.activity.pk in context["Group_Stats"][group.pk]["activity_totals"].keys():
                        context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk]=dict()

                    if not "name" in context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk].keys():
                        context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk]["name"] = dict()

                    context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk]["name"] = activity_num.activity.name
                    if "total" in context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk].keys():
                        print("Adding in "+str(activity_num.amount)+" from "+activity_num.activity.name)
                        context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk]["total"]+= activity_num.amount
                    else:
                        context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk]["total"] = dict()
                        context["Group_Stats"][group.pk]["activity_totals"][activity_num.activity.pk]["total"] = activity_num.amount
        print(context)
        return context

class MemberUpdate(UpdateView):
    model = models.Group
    fields = '__all__'
    template_name = 'TrackerApp/member_update.html'

class PracticeCreate(CreateView):
    model = models.Practice
    fields = ['name', 'description', 'date', 'attendance', "group"]
    template_name = 'TrackerApp/practice_create.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        activity_numbers = models.ActivityNumber.objects.filter(practice__group__pk = self.kwargs['slug'])
        context["activity_totals"] = dict()
        for act_num in activity_numbers:
            context["activity_totals"][act_num.activity.pk]= dict()
            context["activity_totals"][act_num.activity.pk]["name"]=dict()
            context["activity_totals"][act_num.activity.pk]["total"] = dict()

        for act_num in activity_numbers:
            context["activity_totals"][act_num.activity.pk]["name"]=act_num.activity.name
            if context["activity_totals"][act_num.activity.pk]["total"]:
                context["activity_totals"][act_num.activity.pk]["total"]+=act_num.amount
            else:
                context["activity_totals"][act_num.activity.pk]["total"] = act_num.amount
        context["activities"]=models.Activity.objects.filter(group__pk = self.kwargs['slug'])
        print(context)
        return context

    def get_success_url(self, **kwargs):
         group_activities = models.Activity.objects.filter(group__pk = self.kwargs['slug'])
         for activity in group_activities:
             activity_amount= self.request.POST.get(str(activity.pk))
             models.ActivityNumber.objects.create(activity = activity, amount=activity_amount, practice = self.object)

         return super().get_success_url()

class PracticeDetail(DetailView):
    model = models.Practice
    fields = '__all__'
    template_name = 'TrackerApp/practice_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        group_size = len(self.get_object().group.members.all())
        if (group_size):
            context['attendance_rate'] = (len(self.get_object().attendance.all())/group_size) * 100
        return context

class PracticeUpdate(UpdateView):
    model = models.Practice
    fields = '__all__'
    template_name = 'TrackerApp/practice_update.html'

class ActivityCreate(CreateView):
    model = models.Activity
    fields = '__all__'
    template_name = 'TrackerApp/activity_create.html'

    def get_context_data(self,**kwargs):
        context = super().get_context_data(**kwargs)
        print(context)
        return context

class ActivityDetail(DetailView):
    model = models.Activity
    fields = '__all__'
    template_name = 'TrackerApp/activity_detail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        activity_numbers = models.ActivityNumber.objects.filter(activity = self.kwargs['pk'])
        act_amount =0

        for act_num in activity_numbers:
            act_amount += act_num.amount
            context['activity_amount'] =act_amount

        return context

    # last time activity was completed
    # numbers of times completed in past month

class ActivityUpdate(UpdateView):
    model = models.Activity
    fields = '__all__'
    template_name = 'TrackerApp/activity_update.html'

#users signs up for a profile
def signup(request):
    if request.method == 'POST':
        form = forms.SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.save()
            current_site = get_current_site(request)

            #send user registration email with activation link
            # message = render_to_string('registration/acc_active_email.html', {
            #     'user':user,
            #     'domain':current_site.domain,
            #     'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            #     'token': tokens.account_activation_token.make_token(user),
            # })
            # mail_subject = 'Activate your Group Tracker account.'
            # to_email = form.cleaned_data.get('email')
            # subject = "Group Tracker Account Activation"
            # email = EmailMessage(subject, message, to=[to_email])
            # email.send()
            return render(request, 'TrackerApp/index.html')
    else:
        form = forms.SignUpForm()
    return render(request, 'registration/signup.html', {'form': form})

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and tokens.account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user)
        return redirect('index')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')
